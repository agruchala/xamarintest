using System;
using Xamarin.Forms;
using Plugin.Geolocator;
using System.Diagnostics;
using TestApp;
using System.Collections.ObjectModel;

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
namespace WeatherFormsApp
{
	public partial class WeatherFormsAppPage : ContentPage
	{
		private WeatherManager manager = new WeatherManager();
		private ObservableCollection<ForecastData> forecast = new ObservableCollection<ForecastData>();
		private Unit weatherUnit { get; set; }

		public WeatherFormsAppPage()
		{
			InitializeComponent();
			this.Appearing += CP_Appearing;
			ForecastView.ItemsSource = forecast;


		}


		async void OnWeather(object sender, EventArgs e)
		{
			try
			{
				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
				var x = new Coordinate(position.Latitude, position.Longitude);
				//var weather = await manager.getWeatherData(x);
				var recievedForecast = await manager.getFiveDayForecast(x,10);
				forecast.Clear();
				foreach (var item in recievedForecast.forecastData)
				{
					item.mainData.unit = this.weatherUnit;
					forecast.Add(item);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			}
		}

		private void CP_Appearing(object sender, EventArgs e)
		{
			OnWeather(null, null);
		}
	}
}
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously