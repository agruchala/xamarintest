﻿using System;
namespace WeatherFormsApp
{
	public enum Unit 
	{
		Metric,
		Imperial,
		Kelvin 
	};


	static class UnitExtension 
	{
		public static String getString(this Unit unit) 
		{
			switch (unit) 
			{ 
				case Unit.Metric:
					return " ℃";
				case Unit.Imperial:
					return " ℉";
				default:
					return "";
			}
		}
	}
}
