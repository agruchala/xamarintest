﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class City
	{
		
		public City()
		{
		}

		[JsonProperty]
		public int id;

		[JsonProperty]
		public string name;

		[JsonProperty("coord")]
		public Coordinate coordinate;
	}
}
