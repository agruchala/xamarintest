﻿using System;
using Newtonsoft.Json;
using WeatherFormsApp;

namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class MainData
	{
		[JsonProperty("temp")]
		public double Temperature{ get; set; }

		[JsonProperty("pressure")]
		public double pressure{ get; set; }

		[JsonProperty("humidity")]
		public double humidity{ get; set; }

		[JsonProperty("temp_min")]
		public double minimalTemperature{ get; set; }

		[JsonProperty("temp_max")]
		public double maximalTemperature{ get; set; }

		[JsonProperty("sea_level")]
		public double pressureSeaLevel{ get; set; }

		[JsonProperty("grnd_level")]
		public double pressureGroundLevel{ get; set; }

		public Unit unit = Unit.Kelvin;

		public String TemmeratureString { 
			get 
			{
				return $"{Temperature}{unit.getString()}";
			}
		}

		public MainData()
		{
			
		}
	}
}
