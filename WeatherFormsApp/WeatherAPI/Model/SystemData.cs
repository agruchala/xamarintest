﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class SystemData
	{
		[JsonProperty]
		public int type;

		[JsonProperty]
		public int id;

		[JsonProperty]
		public double message;

		[JsonProperty]
		public string country;

		[JsonProperty]
		[JsonConverter(typeof(MicrosecondEpochConverter))]
		public DateTime sunrise;

		[JsonProperty]
		[JsonConverter(typeof(MicrosecondEpochConverter))]
		public DateTime sunset;

		public SystemData()
		{
		}
	}
}
