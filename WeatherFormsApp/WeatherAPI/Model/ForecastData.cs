﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TestApp

{
	[JsonObject(MemberSerialization.OptIn)]
	public class ForecastData
	{
		public ForecastData()
		{
		}

		[JsonProperty("dt")]
		[JsonConverter(typeof(MicrosecondEpochConverter))]
		public DateTime date{ get; set; }

		[JsonProperty("main")]
		public MainData mainData{ get; set; }

		[JsonProperty]
		public List<WeatherData> weather{ get; set; }

		[JsonProperty("wind")]
		public Wind wind{ get; set; }

		[JsonProperty("clouds")]
		public Clouds clouds{ get; set; }

		[JsonProperty("rain")]
		public Rain rain{ get; set; }

		[JsonProperty("snow")]
		public Rain snow{ get; set; }

	}
}
