﻿using System;
using Newtonsoft.Json;
namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Clouds
	{
		[JsonProperty("all")]
		public double all;

		public Clouds()
		{
		}
	}
}
