﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Forecast
	{
		public Forecast()
		{
		}

		[JsonProperty]
		public City city;

		[JsonProperty]
		public string country;

		[JsonProperty("list")]
		public List<ForecastData> forecastData;


	}
}
