﻿using System;
using Newtonsoft.Json;

namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Rain
	{
		[JsonProperty("3h")]
		public double threeHours;

		public Rain()
		{
		}
	}
}
