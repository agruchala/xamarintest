﻿using System;
using Newtonsoft.Json;
                
namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class WeatherData
	{
		[JsonProperty]
		public int id;

		[JsonProperty]
		public string main;

		[JsonProperty]
		public string description;

		[JsonProperty]
		public string icon;

		public WeatherData()
		{
		}
	}
}
