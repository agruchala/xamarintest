﻿using System;
using Newtonsoft.Json;
namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Coordinate
	{
		[JsonProperty("lat")]
		public double latitude;

		[JsonProperty("lon")]
		public double longitude;

		public Coordinate()
		{
		}

		public Coordinate(double latitude, double longitude) {
			this.latitude = latitude;
			this.longitude = longitude;
		}
	}
}
