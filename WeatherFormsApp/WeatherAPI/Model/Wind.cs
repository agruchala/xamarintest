﻿using System;
using Newtonsoft.Json;
namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Wind
	{
		[JsonProperty("speed")]
		public double speed;

		[JsonProperty("deg")]
		public double direction;

		public Wind()
		{
		}
	}
}
