﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace TestApp
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Weather
	{
		[JsonProperty("coord")]
		public Coordinate coordinate;

		[JsonProperty("sys")]
		public SystemData system;

		[JsonProperty]
		public List<WeatherData> weather;

		[JsonProperty("main")]
		public MainData mainData;

		[JsonProperty("wind")]
		public Wind wind;

		[JsonProperty("clouds")]
		public Clouds clouds;

		[JsonProperty("rain")]
		public Rain rain;

		[JsonProperty("snow")]
		public Rain snow;

		[JsonProperty("dt")]
		[JsonConverter(typeof(MicrosecondEpochConverter))]
		public DateTime calculationDate;

		[JsonProperty("name")]
		public string cityName;

		public Weather()
		{
			
		}
	}
}
