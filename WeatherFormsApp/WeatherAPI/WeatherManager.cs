﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace TestApp
{
	public class WeatherManager
	{
		private RequestBuilder builder = new RequestBuilder();
		public WeatherManager()
		{
		}

		public async Task<Weather> getWeatherData(Coordinate coordinate, Action<Weather> callback = null) {
			var weather = await builder.getCurrentWeather(coordinate);
			if (callback != null)
			{
				callback(weather);
			}
			return weather;
		}

		public async Task<Forecast> getFiveDayForecast(Coordinate coordinate, int count = 5, Action<Forecast> callback = null) {
			var forecast = await builder.getFiveDayForecast(coordinate, count);
			if (callback != null)
			{
				callback(forecast);
			}
			return forecast;
		}

	}
}
