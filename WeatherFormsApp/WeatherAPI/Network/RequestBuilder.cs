using System;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;

namespace TestApp
{
	internal class RequestBuilder
	{
		private static readonly string baseUri = "http://api.openweathermap.org/data/2.5";


		public RequestBuilder()
		{
		}

		internal async Task<Weather> getCurrentWeather(Coordinate coordinate) {

			return await baseUri.AppendPathSegment("weather")
				                .SetQueryParams(new { lat = coordinate.latitude, lon = coordinate.longitude, APPID = APIConstants.API_KEY })
				                .GetJsonAsync<Weather>();

		}

		internal async Task<string> getCurrentWeatherString(Coordinate coordinate) { 
			return await baseUri.AppendPathSegment("weather")
								.SetQueryParams(new { lat = coordinate.latitude, lon = coordinate.longitude, APIConstants.API_KEY })
				                .GetStringAsync();
		
		}

		internal async Task<Forecast> getFiveDayForecast(Coordinate coordinate, int count = 5) {
			return await baseUri.AppendPathSegment("forecast")
								.SetQueryParams(new { lat = coordinate.latitude, lon = coordinate.longitude, APPID = APIConstants.API_KEY, cnt = count, units = "metric" })
				                .GetJsonAsync<Forecast>();
		}

	}
}